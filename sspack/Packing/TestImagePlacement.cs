﻿namespace sspack
{
    class TestImagePlacement
    {
        private readonly ImageDetail _imageDetail;
        private readonly RectanglePlacement _placement;

        public TestImagePlacement(ImageDetail imageDetail, RectanglePlacement placement)
        {
            _imageDetail = imageDetail;
            _placement = placement;
        }

        public void ConfirmPlacement()
        {
            _imageDetail.Location = _placement.Location;
            _imageDetail.IsRotated = _placement.IsRotated;
        }
    }
}