#region MIT License

/*
 * Copyright (c) 2009-2010 Nick Gravelyn (nick@gravelyn.com), Markus Ewald (cygon@nuclex.org)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 * 
 */

#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace sspack
{
	public class ImagePacker
	{
		// various properties of the resulting image
		private ImagePackOptions _options;
		private int outputWidth, outputHeight;

	    private List<ImageDetail> _imageDetails;

		/// <summary>
		/// Packs a collection of images into a single image.
		/// </summary>
		/// <param name="options">The packing options.</param>
		/// <param name="outputImage">The resulting output image.</param>
		/// <param name="outputMap">The resulting output map for the images.</param>
		/// <returns>0 if the packing was successful, error code otherwise.</returns>
		/// 
		public int PackImage(
			ImagePackOptions options,
			out Bitmap outputImage, 
			out IEnumerable<ImageDetail> outputMap
			)
		{
			_options = options;

			outputWidth = _options.MaximumWidth;
			outputHeight = _options.MaximumHeight;

			outputImage = null;
			outputMap = null;

            // initialise image details
            _imageDetails = new List<ImageDetail>();
            foreach (var fileName in options.ImageFiles)
            {
                var imageDetail = new ImageDetail(fileName);
                if (imageDetail.Bitmap == null)
                    return (int)FailCode.FailedToLoadImage;
                _imageDetails.Add(imageDetail);
                AddAnimationFrames(imageDetail);
            }
            Console.WriteLine(_imageDetails.Count + " total image frames found.");

		    if (options.AutoCrop)
		    {
                // crop images
                if (options.AutoCropSame)
                {
                    var cropRects = _imageDetails
                        .Select(imageDetail => CropFindSize(imageDetail.Bitmap))
                        .ToList();
                    var commonCropRect = MinimumCropThatContainsAll(cropRects);
                    foreach (var imageDetail in _imageDetails)
                        imageDetail.CropImage(commonCropRect);
                }
                else
                {
                    foreach (var imageDetail in _imageDetails)
                        imageDetail.CropImage(CropFindSize(imageDetail.Bitmap));
                }
            }

			// sort our files by file size so we place large sprites first
            _imageDetails = _imageDetails
		        .OrderByDescending(imageDetail => Math.Max(imageDetail.Bitmap.Width, imageDetail.Bitmap.Height))
		        .ThenByDescending(imageDetail => Math.Min(imageDetail.Bitmap.Width, imageDetail.Bitmap.Height))
		        .ToList();

			// try to pack the images
			if (!PackImageRectangles())
				return (int)FailCode.FailedToPackImage;

			// make our output image
			outputImage = CreateOutputImage();
			if (outputImage == null)
				return (int)FailCode.FailedToSaveImage;

			if (options.GenerateMap)
			{
				outputMap = _imageDetails;
			}

			// clear our dictionaries just to free up some memory
            _imageDetails = null;

			return 0;
		}

	    private void AddAnimationFrames(ImageDetail imageDetail)
	    {
	        if (!imageDetail.Bitmap.FrameDimensionsList.Contains(FrameDimension.Time.Guid))
                return;

	        var frameCount = imageDetail.Bitmap.GetFrameCount(FrameDimension.Time);
	        if (frameCount <= 1)
                return;

            // add all individual frames
	        for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
	            _imageDetails.Add(new ImageDetail(imageDetail, frameIndex));

	        // remove original which had all frames
            imageDetail.Bitmap.Dispose();
            _imageDetails.Remove(imageDetail);
	    }

		private bool PackImageRectangles()
		{
			int testWidth = _options.MaximumWidth;
			int testHeight = _options.MaximumHeight;

			// try to pack the images into our current test size
            var testImagePlacements = new List<TestImagePlacement>();
            if (!TestPackingImages(testWidth, testHeight, testImagePlacements))
			{
                Console.WriteLine("Failed");
				return false;
			}

			// use our test results
			foreach (var testImagePlacement in testImagePlacements)
                testImagePlacement.ConfirmPlacement();

			// figure out the smallest bitmap that will hold all the images
			testWidth = testHeight = 0;
            int area = 0;
			foreach (var imageDetail in _imageDetails)
			{
				testWidth = Math.Max(testWidth, imageDetail.Location.X + imageDetail.Bitmap.Width);
                testHeight = Math.Max(testHeight, imageDetail.Location.Y + imageDetail.Bitmap.Height);
                area += (imageDetail.Bitmap.Width + _options.ImagePadding) * (imageDetail.Bitmap.Height + _options.ImagePadding);
			}
            Console.WriteLine("Succeeded (" + testWidth + "x" + testHeight + ", " +
                (area / (float)((testWidth + _options.ImagePadding) * (testHeight + _options.ImagePadding)) * 100).ToString("0.00") + "% efficiency)");

			// if we require a power of two texture, find the next power of two that can fit this image
			if (_options.RequirePowerOfTwo)
			{
				testWidth = MiscHelper.FindNextPowerOfTwo(testWidth);
				testHeight = MiscHelper.FindNextPowerOfTwo(testHeight);
			}

			// if we require a square texture, set the width and height to the larger of the two
			if (_options.RequireSquareImage)
			{
				int max = Math.Max(testWidth, testHeight);
				testWidth = testHeight = max;
			}

			outputWidth = testWidth;
			outputHeight = testHeight;

		    return true;
		}

		private bool TestPackingImages(int testWidth, int testHeight, List<TestImagePlacement> testImagePlacement)
		{
			// create the rectangle packer
			ArevaloRectanglePacker rectanglePacker = new ArevaloRectanglePacker(testWidth + _options.ImagePadding, testHeight + _options.ImagePadding, _options.AllowRotation);

			foreach (var imageDetail in _imageDetails)
			{
				// get the bitmap for this file
                var size = imageDetail.Bitmap.Size;

				// pack the image
				RectanglePlacement placement;
				if (!rectanglePacker.TryPack(size.Width + _options.ImagePadding, size.Height + _options.ImagePadding, out placement))
				{
					return false;
				}

				// add the destination rectangle to our dictionary
			    testImagePlacement.Add(new TestImagePlacement(imageDetail, placement));
			}

			return true;
		}

		private Bitmap CreateOutputImage()
		{
			try
			{
				Bitmap outputImage = new Bitmap(outputWidth, outputHeight, PixelFormat.Format32bppArgb);

				// draw all the images into the output image
				foreach (var imageDetail in _imageDetails)
				{
                    if (imageDetail.IsRotated)
                        imageDetail.RotateImage();

					var location = imageDetail.Location;
					var bitmap = imageDetail.Bitmap;

					// copy pixels over to avoid antialiasing or any other side effects of drawing
					// the subimages to the output image using Graphics
					for (int x = 0; x < bitmap.Width; x++)
						for (int y = 0; y < bitmap.Height; y++)
							outputImage.SetPixel(location.X + x, location.Y + y, bitmap.GetPixel(x, y));
				}

				return outputImage;
			}
			catch
			{
                return null;
			}
		}

		private Rectangle CropFindSize(Bitmap bmp)
		{
            int top = int.MaxValue, bottom = int.MinValue, left = int.MaxValue, right = int.MinValue;

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    var pixel = bmp.GetPixel(x, y);
                    if (pixel.A > 0)
                    {
                        left = Math.Min(left, x);
                        right = Math.Max(right, x);
                        top = Math.Min(top, y);
                        bottom = Math.Max(bottom, y);
                    }
                }
            }

            if (top == int.MaxValue) // completely transparent image!
                return new Rectangle(0, 0, 1, 1);

            return GetRectangleFromCoords(left, top, right, bottom);
		}

		private Rectangle MinimumCropThatContainsAll(List<Rectangle> sizes)
		{
			if (sizes.Count < 1)
			{
				return Rectangle.Empty;
			}
			int top = int.MaxValue, bottom = int.MinValue, left = int.MaxValue, right = int.MinValue;

			foreach (var size in sizes)
			{
				top = Math.Min(top, size.Top);
				left = Math.Min(left, size.Left);
				bottom = Math.Max(bottom, size.Bottom);
				right = Math.Max(right, size.Right);
			}

			return GetRectangleFromCoords(left, top, right, bottom);
		}

        private static Rectangle GetRectangleFromCoords(int left, int top, int right, int bottom)
        {
            return new Rectangle(left, top, right - left + 1, bottom - top + 1);
        }
	}
}