﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace sspack
{
    public class MonoGameTxtExporter : IMapExporter
    {
        public string MapExtension
        {
            get { return "MonoGameTxt"; }
        }

        public void SetNames(IEnumerable<ImageDetail> mapDetails)
        {
            var imageDetails = mapDetails as IList<ImageDetail> ?? mapDetails.ToList();

            var fullPaths = imageDetails
                .Select(imageDetail => imageDetail.FullPath)
                .ToList();
            var commonPath = GetCommonFilePath(fullPaths);

            foreach (var imageDetail in imageDetails)
            {
                var folder = (Path.GetDirectoryName(GetPathWithoutCommonPath(imageDetail.FullPath, commonPath)) ?? "");
                folder = folder.Replace(Path.DirectorySeparatorChar, '/');
                if ((folder.Length > 0) && !folder.EndsWith("/"))
                    folder += "/";
                
                imageDetail.Name = folder + imageDetail.Name;
            }
        }

        public void Save(ExporterDetail exporterDetail)
        {
            using (var writer = new StreamWriter(exporterDetail.MapFileName))
            {
                WriteHeader(writer, exporterDetail);

                var orderedDetails = exporterDetail.MapDetails
                    .OrderBy(imageDetail => imageDetail.Name);
                foreach (var imageDetail in orderedDetails)
                    WriteImageDetail(writer, imageDetail);
            }
        }

        private static void WriteHeader(StreamWriter writer, ExporterDetail exporterDetail)
        {
            writer.WriteLine("#");
            writer.WriteLine("# Sprite sheet data for MonoGame.");
            writer.WriteLine("#");
            writer.WriteLine("# For code to load these sprites into your MonoGame project visit:");
            writer.WriteLine("# https://github.com/RandolphBurt/TexturePacker-MonoGameLoader");
            writer.WriteLine("#");
            writer.WriteLine("# Sprite sheet: " +
                             Path.GetFileName(exporterDetail.ImageFileName) +
                             " (" + exporterDetail.ImageBitmap.Width + " x " + exporterDetail.ImageBitmap.Height + ")");
            writer.WriteLine("#");
            writer.WriteLine("");
        }

        private static void WriteImageDetail(StreamWriter writer, ImageDetail imageDetail)
        {
            var pivotPointX = ((imageDetail.OriginalSize.Width / 2.0) - imageDetail.CropRectangle.X) / imageDetail.CropRectangle.Width;
            var pivotPointY = ((imageDetail.OriginalSize.Height / 2.0) - imageDetail.CropRectangle.Y) / imageDetail.CropRectangle.Height;

            writer.WriteLine(
                "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9}",
                imageDetail.Name,
                imageDetail.IsRotated ? 1 : 0,
                imageDetail.Location.X,
                imageDetail.Location.Y,
                imageDetail.Bitmap.Width,
                imageDetail.Bitmap.Height,
                imageDetail.OriginalSize.Width,
                imageDetail.OriginalSize.Height,
                pivotPointX,
                pivotPointY);
        }

        private static string GetCommonFilePath(IList<string> fullPaths)
        {
            if (!fullPaths.Any())
                return null;
            var commonPath = Path.GetDirectoryName(fullPaths[0]) + Path.DirectorySeparatorChar;
            while (!fullPaths.All(f => f.StartsWith(commonPath)))
                commonPath = commonPath.Substring(0, commonPath.LastIndexOf(Path.DirectorySeparatorChar, commonPath.Length - 2) + 1);
            return commonPath;
        }

        private static string GetPathWithoutCommonPath(string fullPath, string commonPath)
        {
            return fullPath.Substring(commonPath.Length);
        }
    }
}